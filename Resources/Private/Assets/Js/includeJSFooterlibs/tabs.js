jQuery(document).ready(function($){

    // Allow Page URL to activate a tab's ID
    var taburl = document.location.toString();
    if( taburl.match('#tab-page_') ) {
        $('.nav-tabs a[href=#'+taburl.split('#')[1]+']').tab('show');
        $('html, body').animate({
            scrollTop: $('.tx-hive-cpt-cnt-bs-tab-collapse').offset().top
        }, 2000);
    }

    // Allow internal links to activate a tab.
    $('.tab-content a[data-toggle="tab"]').click(function (e) {
        e.preventDefault();
        $('a[href="#' + $(this).attr('href').split('#')[1] + '"]').tab('show');
    });
    
    // Add icon for open/close
    $('.tx-hive-cpt-cnt-bs-collapse .card-header h4.panel-title a').click(function(){
        if( $(this).parent().hasClass('accordion--open') ){
            $(this).parent().removeClass('accordion--open');
        }
        else{
            $(this).parent().addClass('accordion--open');

        }

    });
    
    // Lazy loading for accordion activated
    $("a[data-toggle='collapse']").on("click", function() {
        setTimeout(function() {
            bLazy.revalidate();
        }, 500);
    });

}); // End