<?php
defined('TYPO3') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HiveCptCntBsTabCollapse',
            'Hivecptcntbstabcollapsetabrendertab',
            'hive_cpt_cnt_bs_tab_collapse :: Tab :: RenderTab'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HiveCptCntBsTabCollapse',
            'Hivecptcntbstabcollapsecollapserendercollapse',
            'hive_cpt_cnt_bs_tab_collapse :: Collapse :: RenderCollapse'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_bs_tab_collapse', 'Configuration/TypoScript', 'hive_cpt_cnt_bs_tab_collapse');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbstabcollapse_domain_model_tab', 'EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_csh_tx_hivecptcntbstabcollapse_domain_model_tab.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbstabcollapse_domain_model_tab');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbstabcollapse_domain_model_collapse', 'EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_csh_tx_hivecptcntbstabcollapse_domain_model_collapse.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbstabcollapse_domain_model_collapse');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
