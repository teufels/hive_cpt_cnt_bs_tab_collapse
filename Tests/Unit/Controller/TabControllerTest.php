<?php
namespace HIVE\HiveCptCntBsTabCollapse\Tests\Unit\Controller;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use HIVE\HiveCptCntBsTabCollapse\Controller\TabController;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use HIVE\HiveCptCntBsTabCollapse\Domain\Repository\TabRepository;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class TabControllerTest extends UnitTestCase
{
    /**
     * @var TabController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(TabController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllTabsFromRepositoryAndAssignsThemToView()
    {

        $allTabs = $this->getMockBuilder(ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $tabRepository = $this->getMockBuilder(TabRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $tabRepository->expects(self::once())->method('findAll')->will(self::returnValue($allTabs));
        $this->inject($this->subject, 'tabRepository', $tabRepository);

        $view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('tabs', $allTabs);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
