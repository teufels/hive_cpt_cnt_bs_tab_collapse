<?php
namespace HIVE\HiveCptCntBsTabCollapse\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use HIVE\HiveCptCntBsTabCollapse\Domain\Model\Tab;
/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class TabTest extends UnitTestCase
{
    /**
     * @var Tab
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new Tab();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );
    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRenderPageIdReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRenderPageId()
        );
    }

    /**
     * @test
     */
    public function setRenderPageIdForStringSetsRenderPageId()
    {
        $this->subject->setRenderPageId('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'renderPageId',
            $this->subject
        );
    }
}
