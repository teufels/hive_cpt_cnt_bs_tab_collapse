
plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab {
    view {
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse {
    view {
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntbstabcollapse {
    settings {
        production {
            includePath {
            public = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Public/
                private = EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/
                frontend {
                public = typo3conf/ext/hive_cpt_cnt_bs_tab_collapse/Resources/Public/
                }
            }
        }
    }
}