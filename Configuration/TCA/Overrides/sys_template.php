<?php
defined('TYPO3') or die();

$extKey = 'hive_cpt_cnt_bs_tab_collapse';
$title = '[HIVE] Bootstrap Tab/Collapse';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbstabcollapse_domain_model_tab', 'EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_csh_tx_hivecptcntbstabcollapse_domain_model_tab.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbstabcollapse_domain_model_tab');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntbstabcollapse_domain_model_collapse', 'EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_csh_tx_hivecptcntbstabcollapse_domain_model_collapse.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntbstabcollapse_domain_model_collapse');