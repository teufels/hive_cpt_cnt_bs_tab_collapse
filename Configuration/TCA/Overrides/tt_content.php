<?php
defined('TYPO3') or die();

/***************
 * Plugin
 ***************/
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HiveCptCntBsTabCollapse',
    'Hivecptcntbstabcollapsetabrendertab',
    'hive_cpt_cnt_bs_tab_collapse :: Tab :: RenderTab'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HiveCptCntBsTabCollapse',
    'Hivecptcntbstabcollapsecollapserendercollapse',
    'hive_cpt_cnt_bs_tab_collapse :: Collapse :: RenderCollapse'
);

$extensionName = strtolower('hive_cpt_cnt_bs_tab_collapse');

$pluginName = strtolower('Hivecptcntbstabcollapsetabrendertab');
$pluginSignature = str_replace('_', '', $extensionName) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extensionName . '/Configuration/FlexForms/Tab.xml');

$pluginName = strtolower('Hivecptcntbstabcollapsecollapserendercollapse');
$pluginSignature = str_replace('_', '', $extensionName) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extensionName . '/Configuration/FlexForms/Collapse.xml');