<?php
defined('TYPO3') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HiveCptCntBsTabCollapse',
            'Hivecptcntbstabcollapsetabrendertab',
            [
                \HIVE\HiveCptCntBsTabCollapse\Controller\TabController::class => 'renderTab'
            ],
            // non-cacheable actions
            [
                \HIVE\HiveCptCntBsTabCollapse\Controller\TabController::class => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HiveCptCntBsTabCollapse',
            'Hivecptcntbstabcollapsecollapserendercollapse',
            [
                \HIVE\HiveCptCntBsTabCollapse\Controller\CollapseController::class => 'renderCollapse'
            ],
            // non-cacheable actions
            [
                \HIVE\HiveCptCntBsTabCollapse\Controller\CollapseController::class => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntbstabcollapsetabrendertab {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hive_cpt_cnt_bs_tab_collapse') . 'Resources/Public/Icons/user_plugin_hivecptcntbstabcollapsetabrendertab.svg
                        title = LLL:EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_tab_collapse_domain_model_hivecptcntbstabcollapsetabrendertab
                        description = LLL:EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_tab_collapse_domain_model_hivecptcntbstabcollapsetabrendertab.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab
                        }
                    }
                    hivecptcntbstabcollapsecollapserendercollapse {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hive_cpt_cnt_bs_tab_collapse') . 'Resources/Public/Icons/user_plugin_hivecptcntbstabcollapsecollapserendercollapse.svg
                        title = LLL:EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_tab_collapse_domain_model_hivecptcntbstabcollapsecollapserendercollapse
                        description = LLL:EXT:hive_cpt_cnt_bs_tab_collapse/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_bs_tab_collapse_domain_model_hivecptcntbstabcollapsecollapserendercollapse.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntbstabcollapsetabrendertab >
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntbstabcollapsecollapserendercollapse >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntbstabcollapsetabrendertab {
                            iconIdentifier = bootstrap_cpt_brand_32x32_svg
                            title = Bootstrap Tabs
                            description = Tabs without Flux
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntbstabcollapse_hivecptcntbstabcollapsetabrendertab
                            }
                        }
                        show := addToList(hivecptcntbstabcollapsetabrendertab)
                    }

                }
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntbstabcollapsecollapserendercollapse {
                            iconIdentifier = bootstrap_cpt_brand_32x32_svg
                            title = Bootstrap Collapse
                            description = Collapse without Flux
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntbstabcollapse_hivecptcntbstabcollapsecollapserendercollapse
                            }
                        }
                        show := addToList(hivecptcntbstabcollapsecollapserendercollapse)
                    }

                }
            }'
        );

    }, 'hive_cpt_cnt_bs_tab_collapse'
);