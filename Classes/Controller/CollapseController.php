<?php
namespace HIVE\HiveCptCntBsTabCollapse\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use HIVE\HiveCptCntBsTabCollapse\Domain\Repository\CollapseRepository;
use Psr\Http\Message\ResponseInterface;
/***
 *
 * This file is part of the "hive_cpt_cnt_bs_tab_collapse" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Kr�ger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/
/**
 * CollapseController
 */
class CollapseController extends ActionController
{
    /**
     * collapseRepository
     *
     * @var CollapseRepository
     */
    protected $collapseRepository = null;

    /**
     * action renderCollapse
     *
     * @return void
     */
    public function renderCollapseAction(): ResponseInterface
    {    
        // @extensionScannerIgnoreLine
        $pluginUid = $this->configurationManager->getContentObject()->data['uid'];

        $aCollapsesTemp = $this->settings['collapses'];
        $aCollapses= array();

        /* get only activ slides */
        foreach ($aCollapsesTemp as $collapseTemp) {
            if (boolval($collapseTemp['tab']['inactiv']) === false && $collapseTemp) {
                array_push($aCollapses,$collapseTemp);
            } elseif (boolval($collapseTemp['collapse']['inactiv']) === false && $collapseTemp) {
                array_push($aCollapses,$collapseTemp);
            }
        }

        $this->view->assign('recordUid', $pluginUid);
        $this->view->assign('collapses', $aCollapses);
        $this->view->assign('aSettings', $this->settings);
        return $this->htmlResponse();
    }

    public function injectCollapseRepository(CollapseRepository $collapseRepository): void
    {
        $this->collapseRepository = $collapseRepository;
    }
}
